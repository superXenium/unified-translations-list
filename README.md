# Unified Translations List

A list that matches professional-field words (and expressions) in English to a set of precise, easily-understood, professional and printer/monitor friendly words and expressions, made to set a "ergonomical" standard for software/manual translations.